//
//  HelloWorldViewController.swift
//  Hello World (Week 2)
//
//  Created by Kai siang Tui on 10/31/15.
//  Copyright © 2015 Kai siang Tui. All rights reserved.
//

import UIKit

class HelloWorldViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.view.backgroundColor = UIColor.blackColor()
        
        let helloLabel: UILabel = UILabel(frame: CGRect(x: 10, y: 100, width: 400, height: 50))
        helloLabel.text = "大家好"
        helloLabel.backgroundColor = UIColor.yellowColor()
        helloLabel.textAlignment = NSTextAlignment.Center
        self.view.addSubview(helloLabel)
        
        let YouokLebel: UILabel = UILabel(frame: CGRect(x: 10, y: 200, width: 400, height: 50))
        YouokLebel.text = "How are You?"
        YouokLebel.backgroundColor = UIColor.yellowColor()
        YouokLebel.textAlignment = NSTextAlignment.Center
        self.view.addSubview(YouokLebel)
       
       
//      let YouokLebel: UILabel = UILabel(frame: CGRect(x: 10, y: 200, width: 400, height: 50)) same as CGRect
        
        let hiButton: UIButton = UIButton(frame: CGRect (x: 50, y: 400, width: 50, height: 30))
        hiButton.setTitle("Good", forState: UIControlState.Normal)
        self.view.addSubview(hiButton)
        hiButton.backgroundColor = UIColor.magentaColor()
        
        let byeButton: UIButton = UIButton(frame: CGRect (x: 120, y: 400, width: 50, height: 30))
        byeButton.setTitle("Ok..", forState: UIControlState.Normal)
        self.view.addSubview(byeButton)
        byeButton.backgroundColor = UIColor.magentaColor()

        
        let youKiddingBroButton: UIButton = UIButton(frame: CGRect (x: 200, y: 400, width: 200, height: 30))
        youKiddingBroButton.setTitle("You Kidding bro?", forState: UIControlState.Normal)
        self.view.addSubview(youKiddingBroButton)
        youKiddingBroButton.backgroundColor = UIColor.magentaColor()

        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
